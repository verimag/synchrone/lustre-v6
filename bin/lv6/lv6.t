Test the -help option

  $ lv6 -h
  usage: lv6 [options] <file> | lv6 -help
  
  -n, -node <string>
              Set the main node (all items are compiled if unset)
  -o, --output-file <string>
              Set the output file name
  -dir, --directory <string>
              Set the directory into which output files are generated
  -exec       Interpret the program using RIF conventions for I/O (force -eei)
  -2c, --to-c Generate C code
  -cc, --compile-generated-c
              Try to compile the generated C files (force -2c)
  -2c-exec, --to-c-execute
              Try to execute the generated C files (force -cc).
  -rif        Behave as a rif input file (meaningless without -exec)
  -et, --expand-io-type
              Expand structured types of the main node (impact the rif output only).
              Necessary to use lurette and rdbg in presence of lutin (that only
              knows about basic the types int/bool/real)
  -ec, --expanded-code
              Generate ec programs (force '--expand-nodes --no-when-not --expand-enums --lustre-v4 --no-prefix')
  -version, --version
              Print the current version and exit
  -v, --verbose
              Set the verbose level to 1
  -vl <int>   Set the verbose level
  -h, -help, --help
              Display this message
  -m, -more, --more, --advanced-options
              Show more options
  -dev, --dev-options
              Show experimental/internal options

  $ lv6 -more
  usage: lv6 [options] <file> | lv6 -help
  -ocaml      Generate ocaml glue code that makes it possible to call the lv6 interpreter
              from ocaml with the current set of arguments (with Lv6Run.make)
  --when-on-ident
              Invent ident names so that when only operates on idents (to be able
              to translate enums into ec/v4)
  --no-when-not
              Remove 'when not' statements (for ec) 
  -ei, --expand-iterators
              Expand array iterators (i.e., generate iterator-free code)
  -ee, --expand-enums
              Translate enums using extern types and consts
  -eei, --expand-enums-as-int
              Translate enums into integers (to be kind with data plotters)
  -esa, --expand-structs-and-arrays
              Expand structures and arrays (force '-ei' and '-en')
  -en, --expand-nodes
              Expand all node calls in the main node
  -enc, --expand-node-call <string> 
              Expand the call of the specified node (can be used for several nodes)
  -oite, --optimize-ite
              Transform if/then/else into merge when possible
  -lv4, --lustre-v4
              deprecated: generate Lustre V4 code (force '-ei -ee -esa')
  -rte, --runtime-error
              Generate kind2 contracts to check for runtime errors (force '-lv4')
  -np, --no-prefix
              Do not prefix variable names by their module 
  	(beware: variable names may clash with this option)
  -2cdil, --to-c-dont-inline-predef
              Do not inline predef calls when generating C code
  -2c-ext, --2c-include-ext-types
              Always include <module>_<node>_ext.h in <module>_<node>.h
  -2cil, --2c-inline-loop
              Inline loops (that come from array iterators)
  -2csf, --2c-several-files
              Generate one .c and one .h file per node
  -2cgc, --2c-global-ctx
              Node context allocated as global variable (no "new_ctx" method)
  -dro, --2c-dro
              Generate a .dro file (for luciole)
  -lic, --gen-lic
              Generate lic
  -2cns, --2c-no-switch
              Use if-then-else instead of switches when generating C codes
  --precision Number of digits after ther dot used to print floats in -exec mode
  -dbg, --debug
              <dbg_flag>
              Possible dbg_flag are: eval-const, clock-check, woi, split, poly, noalias, oite, nwn, en, ei, enumasbool, esa, check-mem, kcg, C, ast, exec, deps, clock-check2, typing, lazyc, paranoid, profile

  $ lv6 -dev
  usage: lv6 [options] <file> | lv6 -help
  -2smv, --to-smv
              Generate smv code (work in progress)
  -knc, --keep-nested-calls
              Keep nested calls (use with care: it breaks some programs with
              some options combination)
  -knpc, --keep-nested-predef-calls
              Keep nested calls for predefined operators (use with care: it breaks some programs with
              some options combination)
  -rnc, --remove-nested-calls
              Remove nested calls
  -eeb, --expand-enums-as-bool
              Translate enums into boolean arrays (to be kind with model-checkers)
  -kcg, --generate-scade-lustre
              Generate Lustre code that is compatible with the lustre of scade
  -tlex, --test-lexer
              Test the lexical analysis
  -2cw7, --2c-wcet
              Generate a main file for computing the wcet (force -2c -2cgc)
  -2cmc, --2c-multi-core
              Generate a yaml file required for multi-core code generation
  -2cs, --2c-stack
              Transmit Soc I/O as params of the step functions (force -2c)
  -2ch, --2c-heap
              Transmit Soc I/O via a ctx structure in the heap (force -2c)
  -2chs, --2c-heap-and-stack
              Transmit soc memoryless I/O via the stack, and the heap otherwise (force -2c)
  --schedule-simple
              No re-ordering after topological sort
  --schedule-sort
              Sort wrt guard before after topological sort
  --schedule-reorder
              Re-order Soc.gao after scheduling to increase the clock factorisation
  -interface  Print the node interface
  --keep-aliases
              Do not perform aliases elimination
  -unit       Run embedded unit tests
  --nonreg-test
              Avoid printing full path error msgs to ease non-reg test decision
  --gen-autotest
              Generate a Lutin Stimulator and a Lustre oracle to compare the
              result of 2 Lustre compilers
