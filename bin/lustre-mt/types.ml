(* Time-stamp: <modified the 20/09/2024 (at 16:37) by Erwan Jahier> *)

(** type definition, used to store data *)

type variable = {
  mutable name: string;
  mutable var_type: string;
  mutable var_type_end: string (* for arrays, the size part *)
}

type main_node = {
  mutable name: string;
  mutable ctx_type: string;
  mutable ctx_new: string;
  mutable var_in: variable list;
  mutable var_out: variable list;
  mutable ch_in: int list;
  mutable ch_out: int list
}

type node = {
  mutable file_name: string;
  mutable fct_name: string;
  mutable ctx: bool;
  mutable ctx_tab: string
}

type instance = {
  mutable id: int;
  mutable node: node;
  mutable var_in: variable list;
  mutable var_out: variable list;
  mutable ch_in: int list;
  mutable ch_out: int list
}

type task = {
  mutable name: string;
  mutable var_in: variable list;
  mutable var_out: variable list;
  mutable memory: bool;
}
