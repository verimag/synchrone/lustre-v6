set -x
if [ -z ${C_COMPILER} ]; then C_COMPILER=gcc; fi
if [ -z ${MAIN_FILE} ]; then MAIN_FILE=multicore_multicore_omp_test.c; fi
$C_COMPILER -fopenmp -o multicore.exec \
	 lustre_consts.c multicore_multicore_test.c  ${EXTRA_C_FILES} multicore_multicore_loop_io.c ${MAIN_FILE} ${C_LIBS}

