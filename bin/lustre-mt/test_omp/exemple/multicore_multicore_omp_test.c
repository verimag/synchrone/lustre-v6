#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <omp.h>
#include <semaphore.h>
#include <errno.h>
#include "multicore_multicore.h"
#include "multicore_multicore_loop_io.h"
#include "multicore_multicore_omp.h"

/* Initialize the semaphore with the given count.  */
#define SEM_INIT(sem, v, max_v) sem_init(&(sem), 0, (v))
/* wait for the semaphore to be active (i.e. equal to 1) and then decrement it by one. */
#define SEM_WAIT(sem) while (sem_wait(&sem) != 0 && errno == EINTR) continue
/* make the semaphore active (i.e. equal to 1) */
#define SEM_SIGNAL(sem) sem_post(&(sem))


/* Declare variables */
_integer ii;
multicore_wiz win;
_boolean bt;
_boolean xo;
_integer cd;
_boolean bto;
_boolean btb;
multicore_wiz wa;
_real cc;
_real r1;
_real r2;
_real r;

/* Declare semaphores */
sem_t channelCD;
sem_t channelAC;
sem_t channeltopin;
sem_t channeltopout;
sem_t channelCCin;
sem_t channelCCout;

/* Declare context */
multicore_multicore_ctx_type* ctx;

void go_top() {
    SEM_SIGNAL(channeltopin);
}

void go_C() {
    SEM_SIGNAL(channelAC);
}

void go_CC(_real* o, _real rx) { // go : utile pour passer les variables
    cc = *o;
    r = rx;
    SEM_SIGNAL(channelCCin);
}

void wait_go_C() {
    SEM_WAIT(channelAC);
}

void wait_go_top() {
    SEM_WAIT(channeltopin);
}

void wait_go_CC() { //les sémaphores
    SEM_WAIT(channelCCin);
}

void wait_end_C() { //sémaphores qui amènent à un noeud en particulier (si plusieurs noeuds, plusieurs wait_end s'ils utilisent des vars différentes)
    SEM_WAIT(channelCD);
}

void wait_end_CC(_real* o) { // idem que wait_end_C
    SEM_WAIT(channelCCout);
    *o = cc;
}

void wait_end_top() {
    SEM_WAIT(channeltopout);
}

void end_C() { // signaux pour ouvrir sémaphores liées aux sorties de C
    SEM_SIGNAL(channelCD);
}

void end_CC() { // idem que end C pour CC
    SEM_SIGNAL(channelCCout);
}

void end_top() {
    SEM_SIGNAL(channeltopout);
}

/* Instance loops */
void task_loop_A0() {
    multicore_A_step(win, &wa);
}

void task_loop_B0() {
    multicore_B_step(win, bt, &btb, &r1, &ctx->multicore_B_ctx_tab[0]);
}

void task_loop_B1() {
    multicore_B_step(wa, btb, &bto, &r2, &ctx->multicore_B_ctx_tab[1]);
}

void task_loop_C0() {
    while(true) {
        wait_go_C();
        multicore_C_step(ii, wa, &xo, &cc, &ctx->multicore_C_ctx_tab[0]);
        end_C();
    }
}

void task_loop_CC() {
    while(true) {
        wait_go_CC();
        multicore_CC_step(r, &cc, &ctx->multicore_C_ctx_tab->multicore_CC_ctx_tab[0]);
        end_CC();
    }
}

void task_loop_D0() {
    multicore_D_step(cc, r2, &cd);
}

void task_loop_IO() {
    while(true) {
        get_inputs(ctx, &ii, &win, &bt);
        go_top();
        wait_end_top();
        print_outputs(xo, cd, bto);
    }
}

void task_loop_top() {
    while(true) {
        wait_go_top();
        task_loop_A0();
        go_C();
        task_loop_B0();
        task_loop_B1();
        wait_end_C();
        task_loop_D0();
        end_top();
    }
}

/* Main function */
void main() {
  int _s = 0;
    /* Initialize context */
    ctx = multicore_multicore_ctx_new_ctx(NULL);

    /* Initialize semaphores */
    SEM_INIT(channelCD, 0, 1);
    SEM_INIT(channelAC, 0, 1);
    SEM_INIT(channeltopin, 0, 1);
    SEM_INIT(channeltopout, 0, 1);
    SEM_INIT(channelCCin, 0, 1); //sémaphore pour l'entrée de CC
    SEM_INIT(channelCCout, 0, 1); //sémaphore pour la sortie de CC


    #pragma omp parallel num_threads(4) default(shared)
    {
        #pragma omp single nowait
        {
            task_loop_IO();
        }
        #pragma omp single nowait
        {
            print_rif_declaration();
            /* Main loop 
            if (ISATTY) printf("#step \%d \n", _s+1);
            else if(_s) printf("\n");
            fflush(stdout);
            ++_s;*/
            task_loop_top();
        }
        #pragma omp single nowait
        {
            task_loop_C0();
        }
        #pragma omp single nowait
        {
            task_loop_CC();
        }
    }
}
