(* Time-stamp: <modified the 08/11/2024 (at 13:51) by Erwan Jahier> *)

(* Authors: Antony Zahran, Léandre Lacourt *)

open Printf
open Types
open Parser
open MainArgs

let yaml_file = ref ""


let main () =
  if (Array.length Sys.argv) <= 1 then (
    Arg.usage MainArgs.speclist MainArgs.usage; flush stdout; exit 2
  ) else (
    try Arg.parse MainArgs.speclist (fun s -> yaml_file := s) MainArgs.usage
    with
    | Failure(e) -> print_string e; flush stdout; flush stderr; exit 2
    | e -> print_string (Printexc.to_string e);  flush stdout; exit 2
  );
  let yaml = Yaml_unix.of_file_exn Fpath.(v !yaml_file) in
  Parser.get_data yaml;


  (* creates the files *)
  let cfile = open_out (Filename.concat !MainArgs.output_dir "wraptask.c") in
  let hfile = open_out (Filename.concat !MainArgs.output_dir "wraptask.h") in

  if((List.length !tasks) > 0) then (
    (* includes *)
    fprintf cfile "#include <stdio.h>\n";
    fprintf cfile "#include <pthread.h>\n";
    fprintf cfile "#include <semaphore.h>\n";
    fprintf cfile "#include <errno.h>\n";
    fprintf cfile "#include <time.h>\n";
    fprintf cfile "#include <sys/time.h>\n";
  );
  fprintf cfile "#include \"%s.h\"\n" (Filename.basename (Filename.chop_suffix !yaml_file ".yml"));
  fprintf cfile "\n";

  (* semaphores macro *)
  fprintf cfile "/* Initialize the semaphore with the given count.  */\n";
  fprintf cfile "#define SEM_INIT(sem, v, max_v) sem_init(&(sem), 0, (v))\n\n";
  fprintf cfile "/* wait for the semaphore to be active (i.e. equal to 1) and then decrement it by one. */\n";
  fprintf cfile "#define SEM_WAIT(sem) while (sem_wait(&sem) != 0 && errno == EINTR) continue\n\n";
  fprintf cfile "/* make the semaphore active (i.e. equal to 1) */\n";
  fprintf cfile "#define SEM_SIGNAL(sem) sem_post(&(sem))\n\n";

  fprintf cfile "/* task structures */\n";
  let print_task_structs task =
    fprintf cfile "typedef struct {\n";
    fprintf cfile "\t%s_ctx_type ctx;\n" task.name;
    fprintf cfile "\tsem_t sem_start;\n";
    fprintf cfile "\tsem_t sem_join;\n";
    fprintf cfile "\tpthread_t thr;\n}";
    fprintf cfile " %s_TASK_struct;" task.name;
    fprintf cfile "\n\n"
  in
  List.iter print_task_structs !tasks;

  fprintf cfile "/* runners */\n";
  let print_task_runners task =
    fprintf cfile "void* %s_runner(void* cd) {\n" task.name;
    if !verbose then (
      fprintf cfile
         "\tprintf(\"# MT:    Running task %s_TASK\\n\");\n" task.name;
      fprintf cfile "\tfflush(stdout);\n"
    );
    fprintf cfile "\t%s_TASK_struct* ts = (%s_TASK_struct*) cd;\n" task.name task.name;
    fprintf cfile "\twhile(1){\n";
    fprintf cfile "\t\tSEM_WAIT(ts->sem_start);\n";
    if !verbose then (
      fprintf cfile "\t\tprintf(\"# MT:    Step task %s\\n\");\n" task.name;
      fprintf cfile "\t\tfflush(stdout);\n"
    );
    fprintf cfile "\t\t%s_step(&ts->ctx);\n" task.name;
    fprintf cfile "\t\tSEM_SIGNAL(ts->sem_join);\n";
    fprintf cfile "\t}\n";
    fprintf cfile "}\n\n"
  in
  List.iter print_task_runners !tasks;

  fprintf cfile "/* task Initializers */\n";
  let print_task_inits task =
    fprintf cfile "%s_TASK_type %s_TASK_init() {\n" task.name task.name;
    if !verbose then (
      fprintf cfile
        "\tprintf(\"# MT: Initializing task %s_TASK_init (calloc(1,%s))\\n\",sizeof(%s_TASK_struct));\n"
        task.name "%ld"  task.name;
      fprintf cfile "\tfflush(stdout);\n"
    );
    fprintf cfile "\t%s_TASK_struct* ts = (%s_TASK_struct*)calloc(1, sizeof(%s_TASK_struct));\n"
      task.name task.name task.name;
    if task.memory then fprintf cfile "\t%s_ctx_init(&ts->ctx);\n" task.name;
    fprintf cfile "\tSEM_INIT(ts->sem_start, 0, 1);\n";
    fprintf cfile "\tSEM_INIT(ts->sem_join, 0, 1);\n";
    fprintf cfile "\tpthread_create(&(ts->thr), NULL, %s_runner, ts);\n" task.name;
    fprintf cfile "\treturn (void*) ts;\n";
    fprintf cfile "}\n\n"
  in
  List.iter print_task_inits !tasks;

  fprintf cfile "/* task resets */\n";
  let print_task_resets task =
    if task.memory then
      (fprintf cfile "void %s_TASK_reset(%s_TASK_type tD) {\n" task.name task.name;
       fprintf cfile "\t%s_TASK_struct* ts = (%s_TASK_struct*) tD;\n" task.name task.name;
       fprintf cfile "\t%s_ctx_reset(&ts->ctx);\n" task.name;
       fprintf cfile "}\n\n")
    else
      (fprintf cfile "void %s_TASK_reset(%s_TASK_type tD) {\n" task.name task.name;
       fprintf cfile "}\n\n")
  in
  List.iter print_task_resets !tasks;

  fprintf cfile "/* task start */\n";
  let print_task_starts task =
    fprintf cfile "void %s_TASK_START(%s_TASK_type tD) {\n" task.name task.name;
    fprintf cfile "\t%s_TASK_struct* ts = (%s_TASK_struct*) tD;\n" task.name task.name;
    fprintf cfile "\tSEM_SIGNAL(ts->sem_start);\n";
    fprintf cfile "}\n\n"
  in
  List.iter print_task_starts !tasks;

  fprintf cfile "/* task join */\n";
  let print_task_joins task =
    fprintf cfile "void %s_TASK_JOIN(%s_TASK_type tD) {\n" task.name task.name;
    fprintf cfile "\t%s_TASK_struct* ts = (%s_TASK_struct*) tD;\n" task.name task.name;
    fprintf cfile "\tSEM_WAIT(ts->sem_join);\n";
    fprintf cfile "}\n\n"
  in
  List.iter print_task_joins !tasks;

  fprintf cfile "/* task inputs */\n";
  let gen_assign (var:variable) (is_input:bool)  (is_ptr:bool) =
    match is_input, is_ptr with
    | true , true  ->
      fprintf cfile "\tmemcpy(%s, %s, sizeof(%s%s));\n"  ("ts->ctx."^var.name) var.name var.var_type var.var_type_end
    | false, true  ->
      fprintf cfile "\tmemcpy(p%s, %s, sizeof(%s%s));\n" var.name ("ts->ctx."^var.name)  var.var_type var.var_type_end
    | false, false ->
      fprintf cfile "\t*p%s = ts->ctx.%s;\n" var.name var.name
    | true , false ->
      fprintf cfile "\tts->ctx.%s = %s;\n" var.name var.name

  in
  let print_task_inputs task =
    let (print_var_inputs : string -> variable -> unit) =
      fun name var ->
        fprintf cfile "void %s_TASK_setin_%s(%s_TASK_type tD, %s %s%s) {\n"
          name var.name name var.var_type var.name var.var_type_end;
        fprintf cfile "\t%s_TASK_struct* ts = (%s_TASK_struct*) tD;\n" name name;
        gen_assign var true (var.var_type_end <> "");
        fprintf cfile "}\n\n";
    in
    List.iter (print_var_inputs task.name) task.var_in;
  in
  List.iter print_task_inputs !tasks;

  fprintf cfile "/* task outputs */\n";
  let print_task_outputs task =
    let (print_var_outputs : string -> variable -> unit) =
      fun name var ->
        fprintf cfile "void %s_TASK_getout_%s(%s_TASK_type tD, %s* p%s) {\n"
          name var.name name var.var_type var.name (* var.var_type_end *);
        fprintf cfile "\t%s_TASK_struct* ts = (%s_TASK_struct*) tD;\n" name name;
        gen_assign var false (var.var_type_end <> "");
        fprintf cfile "}\n\n";
    in
    List.iter (print_var_outputs task.name) task.var_out;
  in
  List.iter print_task_outputs !tasks;

  fprintf hfile "#ifndef _WRAPTASK_H\n#define _WRAPTASK_H\n";
  fprintf hfile "#include \"lustre_consts.h\" \n\n";

  let print_h_tasks task =
    fprintf hfile "typedef void* %s_TASK_type;\n" task.name;
    fprintf hfile "extern %s_TASK_type %s_TASK_init();\n" task.name task.name;
    fprintf hfile "extern void %s_TASK_reset(%s_TASK_type tD);\n" task.name task.name;
    fprintf hfile "extern void %s_TASK_START(%s_TASK_type tD);\n" task.name task.name;
    fprintf hfile "extern void %s_TASK_JOIN(%s_TASK_type tD);\n" task.name task.name;
    let (print_h_task_inputs : Types.variable -> unit) = fun var ->
      fprintf hfile "extern void %s_TASK_setin_%s(%s_TASK_type tD, %s %s%s);\n"
        task.name var.name task.name var.var_type var.name var.var_type_end
    in
    List.iter print_h_task_inputs task.var_in;
    let (print_h_task_outputs : Types.variable -> unit) = fun var ->
      fprintf hfile "extern void %s_TASK_getout_%s(%s_TASK_type td, %s* %s);\n"
        task.name var.name task.name var.var_type var.name
    in
    List.iter print_h_task_outputs task.var_out
  in
  List.iter print_h_tasks !tasks;
  fprintf hfile "#endif";



(*
*  (* main function *)
*  fprintf cfile "/* Main function */\n";
*  fprintf cfile "void main() {
*  int _s = 0;\n";
*
*  fprintf cfile "    /* Initialize context */\n";
*  fprintf cfile "    ctx = %s(NULL);\n" main.ctx_new;
*  fprintf cfile "\n";
*
*  fprintf cfile "    /* Initialize semaphores */\n";
*  List.iter (fprintf cfile "    SEM_INIT(channel%i, 0, 1);\n") !channels;
*  fprintf cfile "    \n";
*
*  let x = List.length !instances in
*  let i = x+1 in
*  fprintf cfile "\t#pragma omp parallel num_threads(%i) default(shared)\n" i;
*  fprintf cfile "\t{\n";
*  (* main loop *)
*  fprintf cfile "\t\t#pragma omp single nowait\n{\n";
*  fprintf cfile "print_rif_declaration();\n";
*  output_string cfile "    /* Main loop */
* while(true) {
*        if (ISATTY) printf(\"#step \\%d \\n\", _s+1);
*        else if(_s) printf(\"\\n\");
*        fflush(stdout);
*        ++_s;\n";
*  fprintf cfile "        get_inputs(ctx";
*  List.iter (fun (x:variable) -> fprintf cfile ", &%s" x.name) main.var_in;
*  fprintf cfile ");\n";
*
*  List.iter (fprintf cfile "        SEM_SIGNAL(channel%i);\n") main.ch_in;
*  List.iter (fprintf cfile "        SEM_WAIT(channel%i);\n") main.ch_out;
*
*  fprintf cfile "        print_outputs(%s" (List.hd main.var_out).name;
*  List.iter (fun (x:variable) -> fprintf cfile ", %s" x.name) (List.tl main.var_out);
*  fprintf cfile ");\n";
*
*  fprintf cfile "    }\n";
*  fprintf cfile "\t\t}\n";
*  fprintf cfile "    \n";
*
*  let print_instance_thread instance =
*    fprintf cfile "\n#pragma omp single nowait\n{\n";
*    fprintf cfile "loop_%s%i();" instance.node.file_name instance.id;
*    fprintf cfile "}\n";
*  in
*  List.iter print_instance_thread !instances;
*
*  fprintf cfile "\t}\n";
*  fprintf cfile "\n"
*  fprintf cfile "}\n";
*)
;;

let _ = main ()
