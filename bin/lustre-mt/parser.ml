open Types

let tasks = ref []
let name = ref ""

(** saves the data from the Yaml parser into references of type Types.t *)

(* converts a `Float list into a int list *)
let (intlist : Yaml.value list -> int list) =
  fun l ->
  let (yv_to_int : Yaml.value -> int) = fun y ->
    match y with
    |`Float f -> int_of_float f
    |_ -> assert false
  in List.map yv_to_int l
;;



(** let (save_channels : Yaml.value -> unit) = fun x ->
*  match x with
*  |`Float f -> channels := !channels @ [int_of_float f]
*  |_ -> assert false
*
*
* let (save_main : (string * Yaml.value) -> unit) =
*   fun (key, value) ->
*   match key, value with
*   |"name", `String s -> main.name <- s
*   |"ctx_type", `String s -> main.ctx_type <- s
*   |"ctx_new",  `String s -> main.ctx_new  <- s
*   |"var_in",   `A l      -> main.var_in  <- List.map (List.nth !variables) (intlist l)
*   |"var_out",  `A l      -> main.var_out <- List.map (List.nth !variables) (intlist l)
*   |"ch_in",    `A l      -> main.ch_in  <- intlist l
*   |"ch_out",   `A l      -> main.ch_out <- intlist l
*   |_ -> assert false
*
*
*let (save_node_attributes : Types.node -> (string * Yaml.value) -> unit) =
*  fun n (key, value) ->
*  match key, value with
*  |"id", `Float _f -> ()
*  |"file_name", `String s -> n.file_name <- s
*  |"fct_name",  `String s -> n.fct_name <- s
*  |"ctx", `Bool b -> n.ctx <- b
*  |"ctx_tab", `String s -> n.ctx_tab <- s
*  |_ -> assert false
*
*let (save_nodes : Yaml.value -> unit) = fun x ->
*  let t = {name = ""; fct_name = ""; ctx = false; ctx_tab = ""}::[] in
*  match x with
*  |`O l ->
*    List.iter (save_node_attributes (List.hd n)) l;
*    tasks := !tasks @ t
*  |_ -> assert false
*
*
*let (save_instance_attributes : Types.instance -> (string * Yaml.value) -> unit) =
*  fun i (key, value) ->
*  match key, value with
*  |"id",   `Float f -> i.id <- int_of_float f
*  |"node", `Float f -> i.node <- List.nth !nodes (int_of_float f)
*  |"var_in",  `A l -> i.var_in  <- List.map (List.nth !variables) (intlist l)
*  |"var_out", `A l -> i.var_out <- List.map (List.nth !variables) (intlist l)
*  |"ch_in",   `A l -> i.ch_in  <- intlist l
*  |"ch_out",  `A l -> i.ch_out <- intlist l
*  |_ -> assert false
*
*let (save_instances : Yaml.value -> unit) = fun x ->
*  let i = {id = 0; node = List.nth !nodes 0; var_in = []; var_out = []; ch_in = []; ch_out = []}::[]
*  in
*  match x with
*  |`O l ->
*    List.iter (save_instance_attributes (List.hd i)) l;
*    instances := !instances @ i
*  |_ -> assert false *)

let (save_variable_attributes : Types.variable -> (string * Yaml.value) -> unit) =
  fun v (key, value) ->
  match key, value with
  |"name", `String s -> v.name <- s
  |"type", `String s -> v.var_type <- s
  |"type_end", `String s -> v.var_type_end <- s
  | e,_  -> failwith (Printf.sprintf "lustre-mt: the var field is unknown: %s" e)

let (save_variables_in : Types.task -> Yaml.value -> unit) = fun task x ->
  let v = {name = ""; var_type = ""; var_type_end = ""} in
  match x with
  |`O l ->
    List.iter (save_variable_attributes v) l;
    task.var_in <- v :: task.var_in
  |_ -> assert false

let (save_variables_out : Types.task -> Yaml.value -> unit) = fun task x ->
  let v = {name = ""; var_type = ""; var_type_end = ""} in
  match x with
  |`O l ->
    List.iter (save_variable_attributes v) l;
    task.var_out <- v :: task.var_out
  |_ -> assert false


let (save_data_one_task : Types.task -> (string * Yaml.value) -> unit) =
  fun task (key, value) ->
  match key, value with
  |"name", `String n -> task.name <- n
  |"var_in" , `A l -> List.iter (save_variables_in task) l
  |"var_out" , `A l -> List.iter (save_variables_out task) l
  |"memory" , `Bool b -> task.memory <- b
  |_ -> ()

let (save_data_task : Types.task -> (string * Yaml.value) -> unit) =
  fun task (key, value) ->
    match (key, value) with
    | "para_node", `O l -> List.iter (save_data_one_task task) l
    |_ -> ()

let (save_data_tasks : Yaml.value -> unit) = fun x ->
  let task = {name = ""; var_in = []; var_out = []; memory = true} in
  match x with
  |`O l ->
    List.iter (save_data_task task) l;
    tasks := task::!tasks
  |_ -> assert false

let (save_data : (string * Yaml.value) -> unit) = fun (key, value) ->
  match (key, value) with
  |"all_para_nodes", `A l -> List.iter save_data_tasks l;
  |"all_para_nodes", `Null -> assert true
  |"main_node",  `String n -> name := n
  |_ -> assert false

let (get_data : Yaml.value -> unit) = fun x ->
  match x with
  |`O l -> List.iter save_data l
  |_ -> assert false
;;
