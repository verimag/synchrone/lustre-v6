(* Time-stamp: <modified the 31/10/2024 (at 11:45) by Erwan Jahier> *)


let usage = (Sys.argv.(0) ^" [options] f.yml

with -pthread:
  Generates a f_pthread.c file

  exemple of use:
    lv6 f.lus -n node -2cmc
   "^ Sys.argv.(0) ^"f_node.yml
   export C_LIBS=\"-lpthread -lm\"; export MAIN_FILE=\"f_node_pthread.c\"; sh node.sh

")

let verbose = ref false

type generation_mode = Pthread
let generation_mode = ref Pthread
let output_dir = ref "."


(* Cloned from the OCaml stdlib Arg module: I want it on stdout! (scrogneugneu) *)
let usage_out speclist errmsg =
  Printf.printf "%s" (Arg.usage_string speclist errmsg)

let rec speclist =
  [

    "--verbose",Arg.Unit (fun _ -> (verbose := true)),
    " ";
    "-verbose",Arg.Unit (fun _ -> (verbose := true)),
    " ";
    "-verb",Arg.Unit (fun _ -> (verbose := true)),
    "\t\t set on a verbose mode";

    "-pthread",Arg.Unit (fun _ -> (generation_mode := Pthread)),
    "\t\t use pthread (the default)";

    "-dir", Arg.String (fun str -> output_dir := str),
    "\t\t to set the directory of the output files";

    "--help", Arg.Unit (fun _ -> (usage_out speclist usage ; exit 0)),
    " ";
    "-help", Arg.Unit (fun _ -> (usage_out speclist usage ; exit 0)),
    " ";
    "-h", Arg.Unit (fun _ -> (usage_out speclist usage ; exit 0)),
    "\t\t display this help message"
  ]
