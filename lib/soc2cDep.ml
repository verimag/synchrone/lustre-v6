(* Time-stamp: <modified the 14/06/2024 (at 15:50) by Erwan Jahier> *)

open Lv6MainArgs

let gen_assign_var_expr x =
  match global_opt.io_transmit_mode with
    | Ctx -> Soc2cIoCtx.gen_assign_var_expr x
    | CtxArgs -> Soc2cIoCtx.gen_assign_var_expr x
    | Args ->  Soc2cIoArgs.gen_assign_var_expr x

let step_name x =
  match global_opt.io_transmit_mode with
    | Ctx -> Soc2cIoCtx.step_name x
    | CtxArgs -> Soc2cIoCtx.step_name x
    | Args ->  Soc2cIoArgs.step_name x

let get_step_prototype x y =
  match global_opt.io_transmit_mode with
    | Ctx -> Soc2cIoCtx.get_step_prototype x y
    | CtxArgs -> Soc2cIoCtx.get_step_prototype x y
    | Args ->  Soc2cIoArgs.get_step_prototype x  y

let string_of_var_expr x =
  match global_opt.io_transmit_mode with
    | Ctx -> Soc2cIoCtx.string_of_var_expr x
    | CtxArgs -> Soc2cIoCtx.string_of_var_expr x
    | Args ->  Soc2cIoArgs.string_of_var_expr x

let ctx_var x =
  match global_opt.io_transmit_mode with
    | Ctx -> Soc2cIoCtx.ctx_var x
    | CtxArgs -> Soc2cIoCtx.ctx_var x
    | Args ->  Soc2cIoArgs.ctx_var x


let gen_step_call x y para =
  match global_opt.io_transmit_mode with
    | Ctx -> Soc2cIoCtx.gen_step_call x y para
    | CtxArgs -> Soc2cIoCtx.gen_step_call x y para
    | Args ->  Soc2cIoArgs.gen_step_call x y

let inlined_soc x =
  match global_opt.io_transmit_mode with
    | Ctx -> Soc2cIoCtx.inlined_soc x
    | CtxArgs -> Soc2cIoCtx.inlined_soc x
    | Args ->  Soc2cIoArgs.inlined_soc x

let typedef_of_soc x =
  match global_opt.io_transmit_mode with
    | Ctx -> Soc2cIoCtx.typedef_of_soc x
    | CtxArgs -> Soc2cIoCtx.typedef_of_soc x
    | Args -> Soc2cIoArgs.typedef_of_soc x, []


let get_predef_op x =
  match global_opt.io_transmit_mode with
    | Ctx -> SocPredef2cIoCtx.get_predef_op x
    | CtxArgs -> SocPredef2cIoCtx.get_predef_op x
    | Args -> SocPredef2cIoArgs.get_predef_op x
let get_iterator x =
  match global_opt.io_transmit_mode with
    | Ctx -> SocPredef2cIoCtx.get_iterator x
    | CtxArgs -> SocPredef2cIoCtx.get_iterator x
    | Args -> SocPredef2cIoArgs.get_iterator x
let get_condact x =
  match global_opt.io_transmit_mode with
    | Ctx -> SocPredef2cIoCtx.get_condact x
    | CtxArgs -> SocPredef2cIoCtx.get_condact x
    | Args -> SocPredef2cIoArgs.get_condact x
let get_boolred x =
  match global_opt.io_transmit_mode with
    | Ctx -> SocPredef2cIoCtx.get_boolred x
    | CtxArgs -> SocPredef2cIoCtx.get_boolred x
    | Args -> SocPredef2cIoArgs.get_boolred x
