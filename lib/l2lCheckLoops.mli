(* Time-stamp: <modified the 05/05/2022 (at 08:35) by Erwan Jahier> *)

(** Check that there is no dependancy loop between equations. 
 
This check is also done during the lic2soc translation, when ordering
equations. But in the ec mode, no soc is generated, and no such check
is done by ec tools either (exexe, ec2c, etc.).

Hence it is necessary to duplicate the work (done in ActionsDeps)
here.  Note that in ec mode, structs and arrays have been expanded,
which makes things easier.

Should I rather use Lic2soc.f to check loops?
No, because Lic2soc is only necessary when generating C code,
and when generating C code, one cannot use the -knc option
which is useful for, e.g., -lv4.
*)

exception Error of (Lxm.t * string * LicPrg.t)

val doit :  LicPrg.t -> unit
