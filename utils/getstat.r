#! /usr/bin/env Rscript

args <- commandArgs(TRUE)
datafilename <- args[1]    
wcet <- as.integer(args[2])

mydata  <- data.frame(val=read.table(datafilename))
attach(mydata)

pdffilename = paste(datafilename,".pdf", sep ="")
pdf(pdffilename)

# require "apt-get install r-cran-ggplot2"
library(ggplot2)


ggplot(data=mydata, aes(x=V1)) +
  geom_histogram(fill="lightblue") +
  geom_vline(aes(xintercept=vals),
     xintercept=wcet,
     linetype=2,
     size=1,
     color="red"
  ) +
 labs(x= "Cycles", y="Cycles counts")


summary(mydata)

print(sprintf("The WCET computed by otawa is %s", wcet))


