(** Time-stamp: <modified the 07/02/2008 (at 11:27) by Erwan Jahier> *)

(*----------------------------------------------------------------------
MODULE : compUtils
------------------------------------------------------------------------
DESCRIPTION :

Utilitaires pour la compil, au dessus de Lxm et Errors

----------------------------------------------------------------------*)

(** Insert an item in the lexeme table. Raise [Compile_error] if already defined. *)


(* TODO: A mettre ailleurs, ou sous un autre nom.  *)
val put_in_tab : 
  string -> ('a, 'b Lxm.srcflagged) Hashtbl.t -> 'a -> 'b  Lxm.srcflagged -> unit

