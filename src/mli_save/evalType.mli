(** Time-stamp: <modified the 05/02/2008 (at 11:24) by Erwan Jahier> *)

(*----------------------------------------------------------------------
	module : EvalType
	date :
------------------------------------------------------------------------
DESCRIPTION :

	Evaluation des expressions de types. Il utilise CheckConst.

PARAMETRES :
   Pour avoir qq chose de g�n�rique, les fonctions
   sont param�tr�es par un "id_solver", qui contient deux fonctions :

   type id_solver = {
      id2const : Ident.t -> Lxm.t -> const_eff
      id2type  : Ident.t -> Lxm.t -> type_eff
   }

   (N.B. on passe le lexeme pour d�ventuels messages d'erreurs)

FONCTION PRINCIPALE : 
	Elle l�ve "Compile_error lxm msg"

	eval_type
      (env : id_solver) 
      (vexp : type_exp)
   -> type_eff

	N.B. une expression de type NE PEUT PAS d�noter un tuple.

----------------------------------------------------------------------*)

(*---------------------------------------------------------------------
eval_type
-----------------------------------------------------------------------
R�le :

Entr�es : id_solver + type_exp

Sorties : type_eff

Effets de bord :
	Compile_error
----------------------------------------------------------------------*)


val f : CompiledData.id_solver -> SyntaxTreeCore.type_exp -> CompiledData.type_eff
