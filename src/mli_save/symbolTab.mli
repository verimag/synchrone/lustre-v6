(** Time-stamp: <modified the 08/02/2008 (at 09:01) by Erwan Jahier> *)

(**********************************************************
Sous-module pour SyntaxTab 
************************************************************

   Une SymbolTab.t est une association ``contextuelle'' entre
des ident simple (string) et ce � quoi ils correspondent.
   N.B. il est inutile de g�rer les idents absolus (pack+nom)
qui sont par d�finition NON-CONTEXTUELS.
   Dans une table, on a 3 espaces de noms, un par nature d'item
(const, type et oper).

***********************************************************)

type 'a hereflagged =
    Here of 'a            (* local items *) 
  | NotHere of Ident.long (* imported items *)

type t

val create : unit -> t

(* Manip de SymbolTab.t *)

(* Recherche d'items *)
val find_type :  t -> Ident.t -> (SyntaxTreeCore.type_info  Lxm.srcflagged) hereflagged
val find_const : t -> Ident.t -> (SyntaxTreeCore.const_info Lxm.srcflagged) hereflagged
val find_node :  t -> Ident.t -> (SyntaxTreeCore.node_info  Lxm.srcflagged) hereflagged

(* Ajout de nom d'item import�s (via uses) *)
val add_import_const : t -> Ident.t -> Ident.long -> unit
val add_import_type  : t -> Ident.t -> Ident.long -> unit
val add_import_node  : t -> Ident.t -> Ident.long -> unit

(** Add local items declaration *)
val add_type : t -> Ident.t -> SyntaxTreeCore.type_info  Lxm.srcflagged -> unit
val add_const : t -> Ident.t -> SyntaxTreeCore.const_info  Lxm.srcflagged -> unit
val add_node : t -> Ident.t -> SyntaxTreeCore.node_info  Lxm.srcflagged -> unit

(* It�rer sur les items *)

val iter_types : 
 t -> (Ident.t -> (SyntaxTreeCore.type_info Lxm.srcflagged) hereflagged -> unit) -> 
  unit
val iter_consts : 
  t -> (Ident.t -> (SyntaxTreeCore.const_info Lxm.srcflagged) hereflagged -> unit) -> 
  unit
val iter_nodes : 
  t -> (Ident.t -> (SyntaxTreeCore.node_info Lxm.srcflagged) hereflagged -> unit) ->
  unit

val dump : t -> unit

