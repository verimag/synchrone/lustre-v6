(** Time-stamp: <modified the 07/02/2008 (at 11:26) by Erwan Jahier> *)

(** Common to lus2lic and lic2loc  *)

(** Lexeme  *)

type t

val dummy : t

val str    : t -> string
val id     : t -> Ident.t
val line   : t -> int 

(** column numbers *)
val cstart : t -> int
val cend   : t -> int


(** lexer/parser interface
    
    In order to able to compute line and colums numbers, 
    the lexer is supposed to:
    - use Lxm.make to return a lexeme to the parser
    - use newline to add a new line 
*)
val make     : Lexing.lexbuf -> t
val new_line : Lexing.lexbuf -> unit



(** compiler interface *)

(** used to attach a source information to a lexeme *)
type 'a srcflagged = { src : t ; it  : 'a }

val flagit : 'a -> t -> 'a srcflagged

(** Returns the last created lexem. Useful to locate syntax errors. *)
val last_made : unit -> t

(** Erreur/Warning printing *)

val details : t -> string
(** prints something like: 'machin' (line:10, col:3 to 7) *)

val position : t -> string
(** prints something like: line:10, col:3 to 7 *)

