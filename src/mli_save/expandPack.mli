(** Time-stamp: <modified the 07/02/2008 (at 11:25) by Erwan Jahier> *)

(*----------------------------------------------------------------------
MODULE : ExpandPack
------------------------------------------------------------------------
DESCRIPTION :

Une seule fonctionnalit� : transformer au niveau "quasi-syntaxique" des
instances de pack du style "package toto = titi(....)" en package "donn�",
SI BESOIN.

(i.e. pack_info -> pack_given)

----------------------------------------------------------------------*)

(*----------------------------------------------------------------------
MODULE : ExpandPack
------------------------------------------------------------------------
DESCRIPTION :

Entr�e, deux tables d'infos syntaxique :
- une table ptab : (string, SyntaxTree.pack_info srcflagged) Hashtbl.t 
- une table mtab : (string, SyntaxTree.model_info srcflagged) Hashtbl.t 

Sortie, une table d'info de package expans�es :
- une table xptab : (string, t) Hashtbl.t

Fonctionnement :
On met en relation les couples (param formel, arg effectif) :

(type t, id/type_exp) : on cr�e l'alias "type t = id/type_exp",
  qu'on met � la fois dans les export et dans le body
  => LES D�CLARATIONS DE TYPES SONT EXPORT�ES

(const c : t, id/val_exp) : on cr�e l'alias "const c : t = id/val_exp",
  qu'on met � la fois dans les export et dans le body
  => LES D�CLARATIONS DE CONST SONT EXPORT�ES

(node n(..)returns(...), id/node_exp) :
  - on garde le noeud "abstrait" dans export => node n(..)returns(...)
  - on d�finit l'alias "node n(..)returns(...) = id/node_exp" dans body


----------------------------------------------------------------------*)

val doit : 
  (* la table des sources de modeles *)
  (Ident.t, SyntaxTree.model_info Lxm.srcflagged) Hashtbl.t ->
  (* la def de pack � traiter *)
  (SyntaxTree.pack_info  Lxm.srcflagged) ->
  SyntaxTree.pack_given

