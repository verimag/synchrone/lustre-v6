(** Time-stamp: <modified the 07/02/2008 (at 11:25) by Erwan Jahier> *)

(** Pretty-printing the internal structure *)


val packinfo : Format.formatter -> SyntaxTree.pack_info Lxm.srcflagged -> unit
val packbody : Format.formatter -> SyntaxTree.packbody -> unit

val modelinfo : Format.formatter -> SyntaxTree.model_info Lxm.srcflagged -> unit
val op2string : SyntaxTreeCore.predef_node -> string
