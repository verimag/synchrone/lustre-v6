(** Time-stamp: <modified the 30/01/2008 (at 09:47) by Erwan Jahier> *)

(** source info tables: tabulated version of the parse tree 

  - cr��e � partir de la liste des pack/modeles

  - s'occupe de l'instanciation (purement syntaxique) des modeles

  - cr�e pour chaque pack provided la liste ``brute'' des noms d'items
  export�s. Cette liste sera importante pour traiter les "use" lors de
  la cr�ation des tables de symboles de chaque pack
*)

type t

val create : SyntaxTree.pack_or_model list -> t


(* acc�s aux infos *)
val pack_body_env : t -> Ident.pack_name -> SymbolTab.t

(** A package may have no provided part *)
val pack_prov_env : t -> Ident.pack_name -> SymbolTab.t option

(* Liste des noms de packs *)
val pack_list : t -> Ident.pack_name list 


val dump : t -> unit

