(** Time-stamp: <modified the 07/02/2008 (at 11:28) by Erwan Jahier> *)

(*

DESCRIPTION :
	Evaluation statique des expressions "r�put�es" constantes
	(defs de constantes, tailles de tableaux, indices et step des arrays).

PARAMETRES :
	Pour avoir qq chose de g�n�rique, les fonctions
	sont param�tr�es par un "id_solver", qui contient deux fonctions :
	(voir CompiledData) 

	type id_solver = {
		id2const : Ident.idref -> Lxm.t -> const_eff
		id2type : Ident.idref -> Lxm.t -> const_eff
	}
	(N.B. on passe le lexeme pour d�ventuels messages d'erreurs)

FONCTION PRINCIPALE : 
	Elle l�ve "Compile_error lxm msg" en cas d'erreur.

	eval_const
		(env : id_solver) 
		(vexp : val_exp)
	-> const_eff list

	(N.B. dans le cas g�n�ral, une val_exp peut d�noter un tuple, on
	rend donc bien une liste de constante)

FONCTIONS DERIVEES : (permet de pr�ciser les messages d'erreur)
	Elles l�vent "EvalArray_error msg" en cas d'erreur,
	se qui permet de r�cup�rer l'erreur. 

	eval_array_size
		(env : id_solver) 
		(vexp : val_exp)
	-> int

	(N.B. ne renvoie que des taille correctes : > 0)

	eval_array_index
		(env : id_solver) 
		(vexp : val_exp)
		(sz : int)
	-> int 

	(N.B. on doit pr�ciser la taille sz du tableau) 

	eval_array_slice
		(env : id_solver) 
		(sl : slice_info srcflagged)
		(sz : int)
		(lxm : Lxm.t)
	-> slice_eff

	N.B. slice_eff = {first: int; last: int; step: int; width: int}
	N.B. on doit passer le lexeme de l'op�ration � cause d'�ventuels
	warnings
*)


exception EvalArray_error of string

val eval_const : CompiledData.id_solver -> SyntaxTreeCore.val_exp -> 
  CompiledData.const_eff list

(**
   R�le : calcule une taille de tableau 
   
   Entr�es: 

   Sorties :
	int (strictement positif)

   Effets de bord :
	EvalArray_error "bad array size, type int expected but get <t>" si t pas int
	EvalArray_error "bad array size <n>" si n <= 0
*)
val eval_array_size : CompiledData.id_solver -> SyntaxTreeCore.val_exp -> int

(**
   R�le :

   Entr�es :
	id_solver, val_exp, taille du tableau

   Sorties :
	int (entre 0 et taille du tableau -1

   Effets de bord :
	EvalArray_error msg si pas bon
*)
val eval_array_index : CompiledData.id_solver -> SyntaxTreeCore.val_exp -> int -> int

(**
   R�le :

   Entr�es :
	id_solver, slice_info, size du tableau,
	lxm (source de l'op�ration slice pour warning)
   Sorties :
	slice_eff, i.e.
	(fisrt,last,step,width) tels que step <> 0 et
	- si step > 0 alors 0<=first<=last et first<=sz
	- si step < 0 alors 0<=last<=first et first<=sz
	- 1<=width<=sz 
   Effets de bord :
	EvalArray_error msg si pas bon
*)
val eval_array_slice : 
  CompiledData.id_solver -> SyntaxTreeCore.slice_info -> int -> Lxm.t -> 
  CompiledData.slice_eff 
