(** Time-stamp: <modified the 28/01/2008 (at 14:13) by Erwan Jahier> *)

(*----------------------------------------------------------------------
	module : Verbose
	date :
------------------------------------------------------------------------
	description :

	Affichage verbeux avec appel "printf-like" :

Verbose.put "format" args...
----------------------------------------------------------------------*)



val set_level : int -> unit
val get_level : unit -> int

val printf : ?level:int -> ('a, unit, string, unit) format4 -> 'a
val print_string : ?level:int -> string -> unit
