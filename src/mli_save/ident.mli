(** Time-stamp: <modified the 19/12/2007 (at 10:18) by Erwan Jahier> *)


type t

type pack_name

type long


val to_string : t -> string
val of_string : string -> t

val of_long : long -> t
val pack_name_of_string : string -> pack_name
val to_pack_name : t -> pack_name
val pack_name_to_string : pack_name -> string
val pack_of_long : long -> pack_name

val string_of_long : long -> string
val long_to_string : long -> string
val long_of_string : string -> long

val make_long : pack_name -> t -> long


val set_dft_pack_name : pack_name -> unit




(* TODO: a renommer et a abstraire  ??  
   a mettre dans syntaxe.ml ???

   During parsing, we don't know yet what default name we should
   give to the package. Once we know it, we manipulate Ident.t rather than idref

   idref is used to denote user ident, that migth be prefixed
   by the module name or nor. One of the first stage of the compiling 
   will consist in transforming those idref (should be called user_id?)
   into Ident.long

*)
type idref = 
    {
      id_pack : string option;
      id_id  : string
    }

val idref_of_string : string -> idref


val string_of_idref : idref -> string

val name_of_idref : idref -> t
val pack_of_idref : idref -> pack_name option

(** [long_of_idref default_pack_name id_ref] builds a long ident from a 
    SyntaxTree.idref *)
val long_of_idref : idref -> long
