(** Time-stamp: <modified the 04/02/2008 (at 15:39) by Erwan Jahier> *)

(** nb: compiling = type checking + constant evaluation *)


(** A lazy compiler is an internal structure that contains tables
    storing compiled entities (types, const, node). At the beginning
    (on creation), it only contains empty tables. But then, one when
    ask for a type, a const, or a node, the tables are filled in.
*)
type t

(* Create a lazy compiler. *)
val create : SyntaxTab.t -> t


(** Compiles one node *)
val node_check : t -> CompiledData.node_key -> Lxm.t -> CompiledData.node_eff


(* Test/debug des types et constantes statiques associ�es *)
val test : t -> unit
