#include "blink.h"

struct blink_ctx* ctxt = blink_new_ctx(NULL);
byte led1 = 13;
byte led2 = 12;


/*****************
 * Main procedures
 */
void setup() {
     Serial.begin(9600);
     blink_reset(ctxt);
     pinMode(led1, OUTPUT); 
     pinMode(led2, OUTPUT); 

}

void loop() {
    static int step_nb = 0;

    // Set inputs
    // no inputs

    delay(100);   // wait for a little time

    // Do a step
    blink_step(ctxt);
    step_nb++;
}


/*******************
 * Output procedures
 */
void blink_O_led1(void* cdata, _boolean _V) {
  digitalWrite(led1, _V*HIGH); 
}
void blink_O_led2(void* cdata, _boolean _V) {
  digitalWrite(led2, _V*HIGH); 

}

