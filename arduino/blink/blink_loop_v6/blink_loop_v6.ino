#include "blink_blink.h"

blink_blink_ctx_type* ctx = blink_blink_ctx_new_ctx();

byte PIN_led1 = 13;
byte PIN_led2 = 12;

void setup() {
     Serial.begin(9600);
     blink_blink_ctx_reset(ctx);
     pinMode(PIN_led1, OUTPUT); 
     pinMode(PIN_led2, OUTPUT); 
}

void loop() {
  _boolean dummy;
  _boolean led1;
  _boolean led2;

  // Set inputs
  dummy=false;
 
  // Do a step
  blink_blink_step(dummy,&led1,&led2,ctx);

  digitalWrite(PIN_led1, led1); 
  digitalWrite(PIN_led2, led2);

  delay(1000);   // wait for a little time
}


