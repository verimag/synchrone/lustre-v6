(setq load-path (cons (expand-file-name "~/dd/org-mode/share/emacs/site-lisp/") load-path))
(setq load-path (cons (expand-file-name "~/dd/org-mode/lisp/") load-path))


(require 'org-latex)

(setq org-export-latex-listings t)
(add-to-list 'org-export-latex-packages-alist '("" "listings"))
(add-to-list 'org-export-latex-packages-alist '("" "color"))
