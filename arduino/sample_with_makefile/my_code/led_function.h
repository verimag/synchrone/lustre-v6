/* This file was generated by lv6 version master.737 (2727a7744111c84f7984634d2bd3ad6f7c6c7ff9). */
/*  lv6 -2c led.lus -n arduino_puzzle */
/* on estrop the 02/05/2018 at 11:14:43 */

#include <stdlib.h>
#include <string.h>

#include "lustre_types.h"
#include "lustre_consts.h"

#ifndef _led_arduino_puzzle_H_FILE
#define _led_arduino_puzzle_H_FILE
void Lustre_arrow_ctx_reset(Lustre_arrow_ctx_type* ctx);
Lustre_arrow_ctx_type* Lustre_arrow_ctx_new_ctx();
void Lustre_arrow_step(_boolean ,_boolean ,_boolean *,Lustre_arrow_ctx_type*);

void Lustre_pre_ctx_reset(Lustre_pre_ctx_type* ctx);
Lustre_pre_ctx_type* Lustre_pre_ctx_new_ctx();
void Lustre_pre_get(_boolean *,Lustre_pre_ctx_type*);

void Lustre_pre_set(_boolean ,Lustre_pre_ctx_type*);

void led_arduino_puzzle_ctx_reset(led_arduino_puzzle_ctx_type* ctx);
led_arduino_puzzle_ctx_type* led_arduino_puzzle_ctx_new_ctx();
void led_arduino_puzzle_step(_boolean ,_boolean ,_boolean *,_boolean *,_boolean *,_boolean *,_boolean *,led_arduino_puzzle_ctx_type*);

void led_fedge_ctx_reset(led_fedge_ctx_type* ctx);
led_fedge_ctx_type* led_fedge_ctx_new_ctx();
void led_fedge_step(_boolean ,_boolean *,led_fedge_ctx_type*);

void led_puzzle1_ctx_reset(led_puzzle1_ctx_type* ctx);
led_puzzle1_ctx_type* led_puzzle1_ctx_new_ctx();
void led_puzzle1_step(_boolean ,_boolean ,_boolean *,_boolean *,_boolean *,_boolean *,_boolean *,led_puzzle1_ctx_type*);

/////////////////////////////////////////////////
#endif
