
#include <Arduino.h>
#include "led_function.h"

led_arduino_puzzle_ctx_type* ctx = led_arduino_puzzle_ctx_new_ctx();

byte PIN_led1 = 13;
byte PIN_led2 = 12;
byte PIN_led3 = 11;
byte PIN_led4 = 10;
byte PIN_led5 = 9;

byte PIN_red = 4;
byte PIN_blue = 3;

void setup() {
     pinMode(PIN_led1, OUTPUT); 
     pinMode(PIN_led2, OUTPUT); 
     pinMode(PIN_led3, OUTPUT); 
     pinMode(PIN_led4, OUTPUT); 
     pinMode(PIN_led5, OUTPUT);

     pinMode(PIN_red, INPUT);
     pinMode(PIN_blue, INPUT);

     led_arduino_puzzle_ctx_reset(ctx);

     Serial.begin(9600);
}

void loop() {
  _boolean led1;
  _boolean led2;
  _boolean led3;
  _boolean led4;
  _boolean led5;
  _boolean red;
  _boolean blue;
  
// read the pushbutton input pin:
  red  = digitalRead(PIN_red);
  blue = digitalRead(PIN_blue);
 
  // Do a step
  led_arduino_puzzle_step(red,blue,&led1,&led2,&led3,&led4,&led5,ctx);

  digitalWrite(PIN_led1, led1); 
  digitalWrite(PIN_led2, led2);
  digitalWrite(PIN_led3, led3);
  digitalWrite(PIN_led4, led4);
  digitalWrite(PIN_led5, led5);

  delay(50);   // wait a little 
}


